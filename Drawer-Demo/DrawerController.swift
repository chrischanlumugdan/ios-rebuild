//
//  DrawerController.swift
//  Drawer-Demo
//
//  Created by Christian Lumugdan on 17/12/17.
//  Copyright © 2017 Christian Lumugdan. All rights reserved.
//

import UIKit


class DrawerController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    
 
    @IBOutlet var imgUser: UIImageView!
    
    @IBOutlet var viewWidth: NSLayoutConstraint!
    
    var arrayTitles = ["Screen 1","Screen 2","Screen 3"]
    
    var drawerTitle = [DrawerList]()
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let appDel = UIApplication.shared.delegate as! AppDelegate
//
//        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "Screen1")
//        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "Drawer")
//
//        appDel.drawerController.mainViewController = mainVC
//        appDel.drawerController.drawerViewController = menuVC
//        //                appDel.drawerController.screenEdgePanGestureEnabled = false
//
//        appDel.window?.rootViewController = appDel.drawerController
//        appDel.window?.makeKeyAndVisible()
        
        collectionView.delegate = self
        collectionView.dataSource = self
       
        setImage()
        setCollectionView()
        setList()
    }
    
    func setList() {
        self.drawerTitle.append(DrawerList(img: "drawer-1", label: "Find Parking"))
        self.drawerTitle.append(DrawerList(img: "drawer-2", label: "My Account"))
        self.drawerTitle.append(DrawerList(img: "drawer-3", label: "Parking History"))
        self.drawerTitle.append(DrawerList(img: "drawer-4", label: "Dibz Pay"))
        self.drawerTitle.append(DrawerList(img: "drawer-rewards", label: "Dibz Rewards"))
        self.drawerTitle.append(DrawerList(img: "drawer-5", label: "Residential Partner"))
        self.drawerTitle.append(DrawerList(img: "drawer-announcements", label: "Announcements"))
        self.drawerTitle.append(DrawerList(img: "drawer-6", label: "Refer a Friend"))
        self.drawerTitle.append(DrawerList(img: "drawer-1", label: "Support"))
        self.drawerTitle.append(DrawerList(img: "drawer-7", label: "Logout"))
    }
    
    func setImage() {
        imgUser.layer.cornerRadius = imgUser.frame.width/2
        imgUser.clipsToBounds = true
        imgUser.layer.borderWidth = 1
        imgUser.layer.borderColor = UIColor.white.cgColor
        viewWidth.constant = UIScreen.main.bounds.width - 80
    }
    func setCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 55)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return drawerTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customCell", for: indexPath) as! DrawerCollectionViewCell
        cell.imgIcon.image = UIImage(named: drawerTitle[indexPath.row].img)
        cell.lblDesc.text = drawerTitle[indexPath.row].label
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        switch indexPath.row {
        case 0:
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let screen1 = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            appDel.drawerController.mainViewController = screen1
            break
        case 1:
            let screen2 = self.storyboard?.instantiateViewController(withIdentifier: "Screen2") as! ViewController2
            appDel.drawerController.mainViewController = screen2
            break
        default:
            let screen3 = self.storyboard?.instantiateViewController(withIdentifier: "Screen3") as! ViewController3
            appDel.drawerController.mainViewController = screen3
            break
        }
        
        appDel.drawerController.setDrawerState(.closed, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


