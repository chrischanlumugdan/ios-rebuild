//
//  ViewController.swift
//  Drawer-Demo
//
//  Created by Christian Lumugdan on 17/12/17.
//  Copyright © 2017 Christian Lumugdan. All rights reserved.
//

import UIKit
import KYDrawerController

class ViewController: UIViewController {

    
    let appDel = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var visualEffect: UIVisualEffectView!
    
    
    var effect: UIVisualEffect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "Screen1")
        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "Drawer")
        
        appDel.drawerController.mainViewController = self
        appDel.drawerController.drawerViewController = menuVC
        //                appDel.drawerController.screenEdgePanGestureEnabled = false
        
        appDel.window?.rootViewController = appDel.drawerController
        appDel.window?.makeKeyAndVisible()
    }
  
    func blurrView() {
        UIView.animate(withDuration: 0.4) {
            self.visualEffect.effect = self.effect
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnMenuPressed(_ sender: Any) {
        
        appDel.drawerController.setDrawerState(.opened, animated: true)
    }
    
    @IBAction func btnGoBackPressed(_ sender: Any) {
        
        let launchVC = self.storyboard?.instantiateViewController(withIdentifier: "Launch") as! LaunchVC
        
        appDel.window?.rootViewController = launchVC
        appDel.window?.makeKeyAndVisible()
    }
}

