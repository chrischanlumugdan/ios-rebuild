//
//  DrawerCollectionViewCell.swift
//  Drawer-Demo
//
//  Created by Christian Lumugdan on 7/4/18.
//  Copyright © 2018 Christian Lumugdan. All rights reserved.
//

import UIKit

class DrawerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblDesc: UILabel!
    
}
