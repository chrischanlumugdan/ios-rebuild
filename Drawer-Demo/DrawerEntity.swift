//
//  DrawerEntity.swift
//  Drawer-Demo
//
//  Created by Christian Lumugdan on 7/4/18.
//  Copyright © 2018 Christian Lumugdan. All rights reserved.
//

import Foundation

class DrawerList {
    var img: String
    var label: String
    
    required init(img: String, label: String) {
        self.img = img
        self.label = label
    }
}
