//
//  HomeVC.swift
//  Drawer-Demo
//
//  Created by Christian Lumugdan on 7/6/18.
//  Copyright © 2018 Christian Lumugdan. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    let appDel = UIApplication.shared.delegate as! AppDelegate
   
    @IBOutlet var btnHamburger: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuVC = storyboard.instantiateViewController(withIdentifier: "Drawer")
        
        appDel.drawerController.mainViewController = self
        appDel.drawerController.drawerViewController = menuVC
        //                appDel.drawerController.screenEdgePanGestureEnabled = false
        
        appDel.window?.rootViewController = appDel.drawerController
        appDel.window?.makeKeyAndVisible()
    }

    @IBAction func didTapHamburger(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, animations: {
            self.btnHamburger.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI))
             self.view.layoutIfNeeded()
        }, completion: {(finished:Bool) in
            self.appDel.drawerController.setDrawerState(.opened, animated: true)
            self.view.layer.removeAllAnimations()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
